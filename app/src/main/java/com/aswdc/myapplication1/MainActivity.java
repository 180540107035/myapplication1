package com.aswdc.myapplication1;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.aswdc.myapplication1.util.Constant;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    EditText etFirstname, etPhoneNo, etEmail;//etLastname;
    Button btnSubmit;
    //TextView tvDisplay;
    ImageView ivClose;

    RadioGroup rgGender;
    RadioButton rbMale, rbFemale;
    CheckBox cbCricket, cbFootball, cbHockey;
    ImageView ivName, ivPhone;

    ArrayList<HashMap<String, Object>> userList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViewReference();


        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isValid()) {
                    HashMap<String, Object> map = new HashMap<>();
                    map.put(Constant.FIRST_NAME, etFirstname.getText().toString());
                    map.put(Constant.PHONE_NUMBER, etPhoneNo.getText().toString().replace(" ", ""));
                    map.put(Constant.EMAIL_ADDRESS, etEmail.getText().toString().replace(" ", ""));
                    map.put(Constant.GENDER, rbMale.isChecked() ? rbMale.getText().toString() : rbFemale.getText().toString());

                    String hobbies = "";
                    if (cbCricket.isChecked()) {
                        hobbies += "," + cbCricket.getText().toString();
                    }
                    if (cbFootball.isChecked()) {
                        hobbies += "," + cbFootball.getText().toString();
                    }
                    if (cbHockey.isChecked()) {
                        hobbies += "," + cbHockey.getText().toString();
                    }
                    if (hobbies.length() > 0) {
                        hobbies.substring(1);
                    }

                    map.put(Constant.HOBBY, hobbies);

                    userList.add(map);

                    Intent intent = new Intent(MainActivity.this, DisplayActivity.class);
                    intent.putExtra("UserList", userList);
                    startActivity(intent);
                }

                etFirstname.getText().clear();
                etPhoneNo.getText().clear();
                etEmail.getText().clear();
//                rgGender.getText().clear();
//                cbHockey.getText().clear();

//                etFirstname.setText("");
//                etPhoneNo.setText("");
//
//
//                Intent intent = new Intent(MainActivity.this, DisplayActivity.class);
//                String temp = etFirstname.getText().toString();
//                intent.putExtra("value", temp);
//                String phone = etPhoneNo.getText().toString();
//                intent.putExtra("phone", phone);
//                startActivity(intent);

                //Toast.makeText(getApplicationContext(),rbMale.isChecked()?"Male":"Female",Toast.LENGTH_SHORT).show();
            }
        });

        //clear name on click image

        ivName.setOnClickListener((view -> {
            etFirstname.getText().clear();
        }));

        //clear phone no. on click image

        ivPhone.setOnClickListener((view -> {
            etPhoneNo.getText().clear();
        }));

        ivClose.setOnClickListener((view) -> {
            finish();
        });

        rgGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {

                View radioButton = rgGender.findViewById(i);
                int index = rgGender.indexOfChild(radioButton);

                if (i == R.id.rbActMale) {
                    cbCricket.setVisibility(View.VISIBLE);
                    cbFootball.setVisibility(View.VISIBLE);
                    cbHockey.setVisibility(View.VISIBLE);
                } else if (i == R.id.rbActFemale) {
                    cbCricket.setVisibility(View.VISIBLE);
                    cbFootball.setVisibility(View.VISIBLE);
                    cbHockey.setVisibility(View.GONE);

                }
            }
        });
    }


    private void initViewReference() {
        etFirstname = findViewById(R.id.etActivityFirstName);
        //etLastname = findViewById(R.id.etActivityLastName);
        etPhoneNo = findViewById(R.id.etActivityPhoneNumber);
        etEmail = findViewById(R.id.etActivityEmail);

        btnSubmit = findViewById(R.id.btnSubmit);

        ivClose = findViewById(R.id.ivActClose);
        ivName = findViewById(R.id.icActCloseName);
        ivPhone = findViewById(R.id.icActClosePhone);

        rgGender = findViewById(R.id.rgActGender);
        rbMale = findViewById(R.id.rbActMale);
        rbFemale = findViewById(R.id.rbActFemale);

        cbCricket = findViewById(R.id.chbActCricket);
        cbFootball = findViewById(R.id.chbActFootball);
        cbHockey = findViewById(R.id.chbActHockey);

        //etPhoneNo.setText("+91");
    }


    boolean isValid() {
        boolean flag = true;

        if (TextUtils.isEmpty(etFirstname.getText())) {
            etFirstname.setError(getString(R.string.error_enter_value));
            flag = false;
        }
        if (TextUtils.isEmpty(etPhoneNo.getText())) {
            etPhoneNo.setError(getString(R.string.error_enter_value));
            flag = false;
        } else {
            String phoneno = etPhoneNo.getText().toString();
            if (etPhoneNo.length() < 10) {
                etPhoneNo.setError("Enter valid Phone No.");
                flag = false;
            }
        }

        if (TextUtils.isEmpty(etEmail.getText())) {
            etEmail.setError(getString(R.string.error_enter_value));
            flag = false;
        } else {
            String email = etEmail.getText().toString().trim();
            String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
            if (!email.matches(emailPattern)) {
                etEmail.setError("Enter valid Email Address");
                flag = false;
            }
        }


        if (!(cbCricket.isChecked() || cbHockey.isChecked() || cbFootball.isChecked())) {
            Toast.makeText(getBaseContext(), "Please select any one CheckBox", Toast.LENGTH_SHORT).show();
            flag = false;
        }
        return flag;
    }
}

