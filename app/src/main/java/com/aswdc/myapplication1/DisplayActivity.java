package com.aswdc.myapplication1;

import android.os.Bundle;
import android.os.Handler;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.aswdc.myapplication1.adapter.UserListAdapter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

public class DisplayActivity extends AppCompatActivity {

    ListView lvUsers;
    ArrayList<HashMap<String, Object>> userList = new ArrayList<>();
    UserListAdapter userListAdapter;
    boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);
        initViewReference();
        //bindViewValues();
        bindValues();


    }

    private void bindValues() {
        userList.addAll((Collection<? extends HashMap<String, Object>>) getIntent().getSerializableExtra("UserList"));
        userListAdapter = new UserListAdapter(this, userList);
        lvUsers.setAdapter(userListAdapter);
    }


//    void bindViewValues() {
//        Intent intent = getIntent();
//        String result = intent.getStringExtra("tempString");
//        String phone = intent.getStringExtra("phone");
//        textView.setText(result + phone);
//
//        Log.d("result",""+result.length());
//}



    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please Click BACK again to Exit ", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }

        }, 2000);
    }


    void initViewReference() {

        // textView = findViewById(R.id.textDisplay);
        lvUsers = findViewById(R.id.lvActUserList);
    }


}





