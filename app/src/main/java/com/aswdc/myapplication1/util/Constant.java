package com.aswdc.myapplication1.util;

public class Constant {

    public static final String FIRST_NAME = "firstName";
    public static final String PHONE_NUMBER = "phoneNumber";
    public static final String EMAIL_ADDRESS = "emailaddress";
    public static final String GENDER = "gender";
    public static final String HOBBY = "hobby";


}

